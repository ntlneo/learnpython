import requests
import json

BASEURL = "https://api.stackexchange.com/"

endpoint_questions = "/2.3/questions?order=desc&sort=activity&site=stackoverflow"
response = requests.get(BASEURL + endpoint_questions)



#use json lib
# items = response.json()["items"]
# for item in response.json()["items"]:
#     print(item['title'])
# title1 = items[0]['title']
# print(title1)

#not use json lib
print(response.content)
# items = response.content["items"]
# title1 = items[0]['title']
# print(title1)