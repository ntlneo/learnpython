import random
import uuid
import requests
from jsonpath_tp import get, find

BASEURL = "https://todo.pixegami.io"
ENDPOINT_CREATE_TASK = "/create-task"
ENDPOINT_GET_TASK = "/get-task"
ENDPOINT_GET_LISTTASK = "/list-tasks"
ENDPOINT_UPDATE_TASK = "/update-task"
ENDPOINT_DELETE_TASK = "/delete-task"

# payload_UpdateTask = {
#     "content": "update content",
#     "user_id": "update test_user_id",
#     "task_id": "update task_id",
#     "is_done": True,
# }


# Case 1: test call root api
def test_call_APIroot():
    response_Root = requests.get(BASEURL)
    assert response_Root.status_code == 200, "error o dau do"


# Case 2: test create task
def test_createTask():
    payload = get_payload()
    response_CreateTask = create_task(payload)
    dataJson_CreateTask = response_CreateTask.json()

    # dùng jsonpath để Get VALUE qua nhiều cấp KEY
    jsonpath_taskID_CreateTask = "$.task.task_id"
    taskID = get(jsonpath_taskID_CreateTask, dataJson_CreateTask)
    # print(taskID)

    # dùng KEY dict để get VALUE qua nhiều cấp KEY
    # data_taskID = data["task"]["task_id"]
    # print(data_taskID)

    # check create task code = 200
    assert response_CreateTask.status_code == 200

    # check content and userID of GetTask
    response_GetTask = get_task(taskID)
    dataJson_GetTask = response_GetTask.json()
    jsonpath_content_GetTask = "$.content"
    jsonpath_userID_GetTask = "$.user_id"
    content_GetTask = get(jsonpath_content_GetTask, dataJson_GetTask)
    userID_GetTask = get(jsonpath_userID_GetTask, dataJson_GetTask)
    print(content_GetTask)

    assert content_GetTask == payload["content"]
    assert userID_GetTask == payload["user_id"]


def test_getListTask():
    # 0. Prepare data payload: use uuid or random.randint

    # rnum = random.randint(0, 10000)
    id = uuid.uuid4().hex
    data_payload = ["content x", f"userid {id}", "taskid x", False]
    payload = get_payload(*data_payload)

    # 1. create 3 tasks for 1 user
    print()
    numberCreate = 3
    for n in range(numberCreate):
        response_Create = create_task(payload)
        assert response_Create.status_code == 200
        userID = get("$.task.user_id", response_Create.json())

    # 2. verify list task of that user > 3
    userID = payload["user_id"]
    print(userID)
    response_GetTask = get_listTask(userID)
    json = response_GetTask.json()
    jsonpath = "$.tasks[*].task_id"
    listTaskID = [taskid for taskid in find(jsonpath, json)]

    for task in listTaskID:
        print(task)
    # check list task > 0
    assert len(listTaskID) >= numberCreate


def test_updateTask():
    # 1. create a task
    response_Create = create_task(
        get_payload(userID="userid 1", content="content 1", isDone=False)
    )
    assert response_Create.status_code == 200
    taskID = get("$.task.task_id", response_Create.json())
    userID = get("$.task.user_id", response_Create.json())
    print()
    print(taskID)
    print(userID)

    # 2. update task
    data_update = ["updated content 1", "updated userid 1", taskID, True]
    response_Update = update_task(get_payload(*data_update))
    assert response_Update.status_code == 200

    taskid_updated = get("$.updated_task_id", response_Update.json())
    # print(content_updated)
    assert taskid_updated == taskID

    # 3. verify changes when get task -> Bug: userID not change
    response_Get = get_task(taskID)
    content_get = get("$.content", response_Get.json())
    userid_get = get("$.user_id", response_Get.json())
    assert content_get == data_update[0]
    # assert userid_get == data_update[1]


def test_deleteTask():
    # 0. Prepare payload
    data_payload = ["content 2", "userid 2", "taskid 2", False]
    payload = get_payload(*data_payload)

    # 1. create task
    response_Create = create_task(payload)
    assert response_Create.status_code == 200
    taskID = get("$.task.task_id", response_Create.json())
    userID = get("$.task.user_id", response_Create.json())
    print()
    print(taskID)
    print(userID)

    # 2. delete task
    response_Delete = delete_task(taskID)
    assert response_Delete.status_code == 200
    taskID_deleted = get("$.deleted_task_id", response_Delete.json())
    assert taskID_deleted == taskID

    # 3. verify by get task
    response_Get = get_task(taskID)
    assert response_Get != 200
    errorMessage = get("$.detail", response_Get.json())
    print(errorMessage)
    assert errorMessage == f"Task {taskID} not found"


def get_payload(
    content="my test content",
    userID="my test_user_id",
    taskID="my task_id",
    isDone=False,
):
    return {"content": content, "user_id": userID, "task_id": taskID, "is_done": isDone}


def create_task(payload):
    return requests.put(BASEURL + ENDPOINT_CREATE_TASK, json=payload)


def get_task(taskID):
    return requests.get(BASEURL + ENDPOINT_GET_TASK + f"/{taskID}")


def get_listTask(userID):
    return requests.get(BASEURL + ENDPOINT_GET_LISTTASK + f"/{userID}")


def update_task(payload):
    return requests.put(BASEURL + ENDPOINT_UPDATE_TASK, json=payload)


def delete_task(taskid):
    return requests.delete(BASEURL + ENDPOINT_DELETE_TASK + f"/{taskid}")
