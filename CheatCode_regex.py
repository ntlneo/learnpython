"""
import re

***Regex method:
    spit (pattern, string, [re.IGNORECASE])                 : return a list of string that split by the parttern. if matched, list alway contains at least 2 items
    sub (1_word_need_to_replace, 2_word_to_replace, string) : replace 1 by 2
    findall (pattern, string, [re.IGNORECASE])              : return a list of string that match the pattern
    search (pattern, string, [re.IGNORECASE])               : return 1 Match object at anywhere in string that match the pattern
    match (pattern, string, [re.IGNORECASE])                : return 1 Match object at the beginning of string that match the pattern    
    The method re.finditer does the same thing as re.findall, except it returns an iterator, rather than a list.
    
    ***Metacharacter:
    .       : any character
    ^       : start
    $       : end
    |       : or
    -> "^gr.y$"         : has 4 char, start with 'gr', then follow with any character, except a newline, and end with 'y'.     ->grey, grzy
    -> "^m..e$"         : has 4 char, start with 'm', then follow with 2 any character, except a newline, and end with 'e'.    ->maae, mz1e
    -> "BaoAn|ThaoMy"   : has BaoAn or ThaoMy       ->s = "dsafdsaf BaoAn asdfsafsa ThaoMy asdfasdasd BaoAn sa ThaoMydasfds" > BaoAn, ThaoMy, BaoAn, ThaoMy
    
    ***Repeatition metacharacter:
    *       : 0+
    +       : 1+
    ?       : 0 or 1
    {1,4}   : 1 or 2 or 3 or 4. If the first number is missing, it is taken to be zero. If the second number is missing, it is taken to be infinity.      ->{0,1} = ?
    
    -> "egg(spam)*" : has 'egg', then has 'spam' or not.     ->egg, eggzz, eggspamzzz
    -> "g+"         : must has 'g'       ->gzz, gggzzz
    -> "colo(u)?r"  : has 'u' or not            ->color, colour
    -> "9{1,3}$"    : end with 1-3 number 9     ->9, z99, zzz999
    
    ***Character Classes:
    []      : match any character inside []
        - The class [a-z] matches any lowercase alphabetic character.
        - The class [G-P] matches any uppercase character from G to P.
        - The class [0-9] matches any digit.
        - Multiple ranges can be included in one class. For example, [A-Za-z] matches a letter of any case.
        - Place a ^ at the start of a character class to invert it. The metacharacter ^ has no meaning unless it is the first character in a class.
    -> "[aei]"              : has any one of the characters defined 'a', 'e', 'i'.  -> zalsb, zzeivv
    -> "[A-Z][A-Z][0-9]"    : has at least 3 characters as format: first and second are alphabet from A-Z, 3rd is digit  -> LS8adsad, aasdSE3
    -> "[^A-Z]"             : has a none-casesensitive    -> AbCdEfG123, this is all quiet
    
    ***Groups:
    ()              : group a string
        - A call of group(0) or group() returns the whole match.
        - A call of group(n), where n is greater than 0, returns the nth group from the left.
        - The method groups() returns all groups up from 1.
    (?P<name>...)   : Named groups - where name is the name of the group, and ... is the content.
    (?:...).        : Non-capturing groups - They are not accessible by the group method, so they can be added to an existing regular expression without breaking the numbering.
    \1, \99         : This matches the expression of the group of that number.
    -> "egg(spam)*"         : has 'egg', then has 'spam' or not     ->egg, zeggspamz
    -> "a(bc)(de)(f(g)h)i"  : has 'abcdefghi'
    -> "(?P<first>abc)(?:def)(ghi)" : has 'abcdefghi'             ->match.group("first) > abc         ->match.groups() > abc, ghi
    -> "(.+)\1"            : has any word, then repeat that word                 ->word word, ?! ?!

    ***Special Sequences:
    \d  -   \D      : digit                 -       not digit
    \s  -   \S      : whitespace            -       not whitespace
    \w  -   \W      : character(a-zA-Z0-9)  - not character
    \A              : beginning string
    \Z              : end string
    \b  -   \B      : boundary between word and non-word
    -> "(\D+\d)"    : has a not digit, then a digit     ->hi 99!
    
    
"""
import re

# remove special chars
s = "Lam! di. o. nha, ngu"
pattern = r"\W+"
ls = re.split(pattern, s)
print(" ".join(ls))

# remove whitespace and lower chars
s1 = "  zAAB CDDz     sEFFGGd  "
pattern = r"[a-z\s]"
news = re.sub(pattern, "", s1)
print(news)
