



#program show value when input key
car = {
    'brand': "BMW",
    'year' : 2018,
    'color': 'red',
    'mileage': 15000
}

key = input()
print(car[key])

#normal dic
ages = {
    "Dave": 24,
    "Mary": 42,
    "Join": 58
}

print(ages["Mary"])