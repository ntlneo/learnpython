import module_cung_folder  # import module cùng folder

z = module_cung_folder.cong(2, 5)
print(z)

import Folder_Khac.module_khac_folder as mokhac1  # import module khac folder thi Folder.module
from Folder_Khac import module_khac_folder as mokhac2

y1 = mokhac1.tru(10, 5)
y2 = mokhac2.tru(10, 6)
print(y1, y2)
# print(f"{y1} {y2}")

import Package_Khac.module_khac_package as mokhac3  # import module khac Package thi (giống voi khac Folder) Package.module
from Package_Khac import module_khac_package as mokhac4

x1 = mokhac3.nhan(10, 5)
x2 = mokhac4.nhan(10, 6)
print(x1, x2)

import Package_Khac.module_khac_package_coClass as moclass  # import module khac Package có CLASS thi (giống voi khac Folder) Package.module as mo > mo.CLASS
from Package_Khac.module_khac_package_coClass import (
    classKhac as ck,
)  # or use:    from Package.module import CLASS1, CLASS2 ...

c1 = moclass.classKhac.chia(object, 3, 1)
c2 = ck.chia(object, 8, 2)
print(c1, c2)
