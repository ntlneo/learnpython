"""
In Python, the term packaging refers to putting modules you have written in a standard format, so that other programmers can install and use them with ease.
This involves use of the modules setuptools and distutils.

*STEP 1:
The first step in packaging is to organize existing files correctly. 
Place all of the files you want to put in a library in the same parent directory. 
This directory should also contain a file called __init__.py, which can be blank but must be present in the directory.
This directory goes into another directory containing the readme and license, as well as an important file called setup.py.

Example directory structure:
SoloLearn/
   LICENSE.txt
   README.txt
   setup.py
   sololearn/
      __init__.py
      sololearn.py
      sololearn2.py

*STEP 2:
The next step in packaging is to write the setup.py file.
This contains information necessary to assemble the package so it can be uploaded to PyPI and installed with pip (name, version, etc.).
Example of a setup.py file:
from distutils.core import setup

setup(
   name='SoloLearn', 
   version='0.1dev',
   packages=['sololearn',],
   license='MIT', 
   long_description=open('README.txt').read(),
)

*STEP 3:
After creating the setup.py file, upload it to PyPI.

or use the command line to create a binary distribution (an executable installer).
To build a source distribution, use the command line to navigate to the directory containing setup.py, and run the command 
python setup.py sdist
python setup.py bdist_wininst       ->to build a binary distribution.       OR python setup.py bdist
python setup.py sdist upload        ->to upload a package.
python setup.py register
Finally, install a package with python setup.py install.


The previous lesson covered packaging modules for use by other Python programmers. 
However, many computer users who are not programmers do not have Python installed. 
Therefore, it is useful to package scripts as an executable file for the relevant platform, such as the Windows or Mac operating systems.
Window: py2exe, PyInstaller , cx_Freeze 
Mac: py2app, PyInstaller , cx_Freeze
"""


"""
khi gặp câu lệnh IMPORT thì trình biên dịch sẽ tiến hành tìm
kiếm file module tương ứng theo thứ tự thư mục sau:
    1. Thư mục hiện hành mà script đang gọi
    2. Các thư mục trong PYTHONPATH (nếu có set)    , Tạo venv là thêm path vào pythonpath
    3. Các thư mục cài đặt chuẩn trên Linux/Unix..

- FOLDER và PACKAGE: dc đối xử tuong duong nhau khi import và kế thừa
- PACKAGE: có thêm file __init__ , thường thì nó rỗng
    
A Python module is simply a (Python source) file, which can expose classes, functions and global variables.
A Python package is simply a directory of Python module(s).

modules (filenames) should have short, all-lowercase names, and they can contain underscores;
packages (directories) should have short, all-lowercase names, preferably without underscores;
classes should use the CapWords convention.
"""
import math, random  # tải 1 or nhiều module vào lib

print(random.__file__)  # show path của module
print(dir(random))  # show phuong thức và thuộc tính của module
print(dir())  # show phuong thức và thuộc tính của chuong trinh hiện tại

# Dùng lệnh import ...           -> chỉ import module
import math as ma  # dùng as de rút gọn tên

# Dùng lệnh from ... import ...  -> có thể import module or class
from Folder import module as modul
from Package import module as mokhac4
from Package.module import classz as cla
