from file_cung_package_coClass import (
    classCungPackage,
)  # must import class before inherit, cung package thi from FILE(module) import CLASS
from Package_Khac.file_khac_package_coClass import (
    classKhacPackage,
)  # must import class before inherit, khác package thi from PACKAGE.FILE(module) import CLASS


class nay(classCungPackage):  # must import class before inherit
    def __init__(self) -> None:
        pass

    object1 = classCungPackage()
    object1.intong(3, 6)

    object2 = classKhacPackage()
    object2.inhieu(9, 8)


class nay2(classKhacPackage):
    object1 = classKhacPackage()
    object1.inhieu(6, 2)
