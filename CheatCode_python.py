
### 0. List comprehension:


### 1. Dictionary comprehension: -> in ra số:số mũ
new_dict_comp = {n:n**2 for n in range(10) if n%2 == 0}
print(new_dict_comp)
#>> {0: 0, 2: 4, 4: 16, 6: 36, 8: 64}

### 2. Lambda:
print((lambda x: x+10)(3))
#>> 13

result = lambda x,y: x+y
print(result(3,5))
#>> 8

z = lambda x: sum(range(x+1))   #tinh tong tu 0 den x
print(z(4))
#>> 10

### 3. Filter with labda & list comprehension:
### Apply function with every items return list items match with predicate (condition of function is TRUE)
mylist = [1, 5, 4, 6, 8, 11, 3, 12]
listsochan1 = list(filter(lambda t: t%2 == 0, mylist))
listsochan2 = [x for x in mylist if x % 2 == 0]
print(listsochan2)
#>> [4, 6, 8, 12]

"""
###### CS50 note:

*Khi gặp tình huống phải thực hiện lặp đi lặp lại 1 hành động nhiều lần thì nghĩ đến các cách sau:
1. Đệ quy hàm thực hiện hành động đó
2. Loop : Dùng vòng lặp, gọi hàm đó
3. Tạo hàm thực hiện hành động đó, rồi gọi ra xài

- Swap các biến: a,b,c = b,c,a

- print("meow" * 5)
- while True
- try
   except
   else
- return int(input("Please input a numb: "))
- dùng console có param: python x.py var1 var2
- import sys thì sys.argv[0] = filename, [1] = argument in console
- import request
- import json -> json.dump(respond) show pretty Json format
- UnitTest: assert square(3) == 9

- Để sort 1 dict theo value:
for student in sorted(students, key=lambda studentx : studentx['name'], reverse = True)
- list.sort(): sort inplace -> trả về chính list cũ đó
- sorted(list) sort outplace -> trả về new list

- import csv
- reader = csv.Reader(file) -> Dùng đoc từng line
- readerDict =csv.DictReader(file) -> Dùng đọc từng line theo Name của cột dòng đầu tiên : 
for row in readerDict:
 print('line data:', row['name'], row['year'])

*DICT : family= {'lam':1984, 'thanh':1993}
- default sorted(dict) -> chỉ sort theo key
- Sort dict by value: 
family = dict(sorted(family.items(), key=lambda item:item[1]))
- Sort dict by key:
family = dict(sorted(family.items(), key=lambda item:item[0], reverse = False))

*Sort dùng itemgetter:
from operator import itemgetter
Value: sorted(family.items(), key= itemgetter(1))
Key: sorted(family.items(), key= itemgetter(0))

- Assign và so sanh dieu kien cùng lúc
if x := 'i' in text:
 print(x)
 
"""