
// xem tất cả lib đã install
pip list

//

// các lib cần thiết: selenium, appium, pytest
// https://pypi.org/project/selenium/
// https://pypi.org/project/Appium-Python-Client/
// https://pypi.org/project/pytest/
pip install selenium
pip install Appium-Python-Client
pip install pytest

// pytest report html giống allure: https://pypi.org/project/pytest-html-reporter/ 
pip install pytest-html-reporter

// dùng pandas để đọc file excel, csv:
pip install pandas
pip install openpyxl


-----------------------------------------------------------------------------------

// pytest report html thường: https://pypi.org/project/pytest-html/
pip install pytest-html

// nếu dùng Behave thì xài report này khá đẹp
// https://pypi.org/project/behave-html-pretty-formatter/
// https://github.com/behave-contrib/behave-html-pretty-formatter
pip install behave-html-pretty-formatter