
for VScode: select requirements.txt file > click 'Create Venv' . Wait all packages installed > then open new terminal

--------------------------------------------
1. After clone project, use IDE(VScode/Pycharm) terminal to create new venv (virtual environment) from requirements.txt file. Ex: automation_venv
pip install -r requirements.text

2. To use that venv, open cmd at Folder contains automation_venv
automation_venv\Scripts\activate
OR in IDE, select a file .py, 'select interpreter' as that venv

3. Open new IDE terminal, you will see that venv as below
(automation_venv) PS E:\PythonProject\learnpythonseleniumappium>

4. To deactivate venv
(automation_venv) PS E:\PythonProject\learnpythonseleniumappium> deactivate

--------------------------------------------
Python requirements files are a great way to keep track of the Python modules. 
It is a simple text file that saves a list of the modules and packages required by your project. 

*Just creating a Python requirements.txt file. Then save it in the same directory as your .py files for this project.

*You can also generate a Python requirements.txt file directly from the command line with:
pip freeze > requirements.txt		: save as requirements.txt file
pip freeze 							: outputs a list of all installed Python modules with their versions.
pip install -r requirements.txt	: install/update modules
pip uninstall -r requirements.txt	: uninstall modules
pip list --outdated					: output all outdated modules
pip install -U PackageName			: install/upgrade a required package
pip install -U -r requirements.txt	: install/upgrade all required packages
python -m pip check					: check for missing dependencies
*Deactive venv: deactivate

*For auto-gen requirements file, use the pipreqs module. It will scan your IMPORT commands and build a Python requirements file for you. 
(not like pip freeze will add all libs).
pip install pipreqs
pipreqs /home/project/location      -> if you're at project folder, ex: terminal in VScode, just need type:     pipreqs .
-> Successfully saved requirements file in   /home/project/location/requirements.txt
pipreqs <path-to-folder> --print    -> just show libs used in project                                           pipreqs . --print
pipreqs <path-to-folder> --force    -> to update, you must force overwrite an existing requirements.txt file    pipreqs . --force
pipreqs <path-to-folder> --savepath <path-to-file>      
-> save file to another place       -> ex:  pipreqs . --savepath /Users/dradecic/Desktop/r.txt



--------------------------------------------
Goto a Folder will contain venv(ex: E:\VS_Venv), open cmd
If we had already a requirements.txt, we should put it on this same folder
The requirements file is used to install all lib to venv. ex: this requirements.txt here is for selenium-pytest
Create venv:            python -m venv selenium_venv
                        python -m venv E:\VS_Venv\selenium_venv     ->all installed lib wil be in E:\VS_Venv\selenium_venv\Lib\site-packages
Active venv:            selenium_venv\Scripts\activate              ->for window
                        selenium_venv/bin/activate                  ->for Mac/Linux
                        source selenium_venv/bin/activate           ->for Mac/Linux
Deactive venvs:         deactivate                                  ->ex: (automation_venv) E:\VS_Venv>deactivate
show content file       type requirements.txt                       ->for window
                        cat requirements.txt                        ->for Mac/Linux  
View all libs in venv   pip list                                    ->will include pip, setuptools
                        pip freeze                              

Packages need to install in this project: pytest, selenium, webdriver-manager
Below is example with pytest:
install new lib         pip install pytest                   
install exact version   pip install pytest==7.1
upgrade lib             pip install --upgrade pytest
delete lib              pip uninstall pytest
create requirements.txt pip freeze > requirements.txt             ->extract all lib to a requirement file
install all lib         pip install -r requirements.txt