# swap var use assign
a = "lam"
b = "thanh"
c = "an"
d = "my"
print(a, b, c, d)

a, b, c, d = d, c, b, a
print(a, b, c, d)


# swap list use reverse or loop assign
listfamily = ["lam", "thanh", "an", "my", "ben"]
list2 = listfamily.copy()
print(listfamily)

listfamily.reverse()
print(listfamily)

length = len(listfamily)
for i in range(length):
    listfamily[i] = list2[length - 1 - i]
print(listfamily)

# sort value dict use Function
family = {"2lam": 1984, "1thanh": 1993, "3ba": 1957}
print(family)
newfam = sorted(family)  # default is sort key
print(newfam)
print(family.items())
newfam1 = dict(sorted(family.items(), key=lambda item: item[1]))  # sort value
print(newfam1)

# sort value dict use itemgetter
from operator import itemgetter

items = sorted(family.items(), key=itemgetter(1))
print(dict(items))
