import re
# get word from string , phan cách = chữ hoa
a = "RealPythonIsGood"          # expected = Real Python Is Good

#Way 1:
pattern_CharSupper = r"([A-Z])"
substring = re.sub(pattern_CharSupper,r" \1",a)
print(substring.strip())

#Way 2:
pattern_WordSupper = r"[A-Z][^A-Z]*"
listWord = re.findall(pattern_WordSupper,a)
print(" ".join(listWord))