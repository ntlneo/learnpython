import re


def remove_duplicate_whitespace_lowerletter(s: str):
    # use regex - sub to remove whitespace and lowerletter
    pattern = r"[a-z\s]"
    news = re.sub(pattern, "", s)
    print(news)

    # remove duplicate
    z = list(news)
    ls = []
    for c in z:
        if c not in ls:
            ls.append(c)
    kq = "".join(ls)
    print(kq)


remove_duplicate_whitespace_lowerletter("  zAAB CDDz     sEFFGGd  ")
