
list1 = [1,1,"Lam","Thanh_Trung",2,3,"Nguyen",3,4,"Thanh_Trung",5]

#List (nếu list có cả int và str thì dùng sort or sorted sẽ bị lỗi)
#1. Cast List qua Set để tự xoá item trùng
#2. Cast Set qua lại List rồi in ra
def get_list_norepeat():    
    print(f"List ban dau: {list1}")
    x = list(set(list1))
    print(f"List ko trung lap: {x}")
    return x


#List và sort theo str,int
#1. Tạo 2 list rỗng mới cho kiểu int và str
#2. Lặp list cũ, nếu item là kiểu int thì add vào listInt, else thì add vào listStr
#3. In ra kết quả tổng 2 list: listInt + listStr
def get_list_norepeat_sorted():
    list2_norepeat = get_list_norepeat()
    listnum = []
    liststr = []    
    for num in list2_norepeat:
        # print(instanceof(num))
        if isinstance(num,int):
            listnum.append(num)
        else:
            liststr.append(num)
    listnum.sort()
    liststr.sort()
    
    listResult = []
    listResult = liststr + listnum
    print(f"List ko trung lap sorted: {listResult}")
            
            


#Dict trùng value or key:
#1. Tạo dict rỗng mới
#2. Lặp dict cũ, add value vào dict mới nếu value của item trong dict cũ != dict mới
#3. In dict mới
def get_dict_norepeat():    
    dict1 = {'ba': "Lam",
            2: "Thanh",
            1: "Lam",
            "me": 'Thanh',
            "con trai lon": "An",
            "con gai giua": "My",
            "con trai ut": "Duy",
            "ban ba lam": "Duy"                      
            }
    print(f"Dict ban dau: {dict1}")

    dict_noRepeat = {}   
    
    for key, value in dict1.items():
        if value not in dict_noRepeat.values():
            dict_noRepeat[key] = value
    
    print(f"Dic ko trung lap: {dict_noRepeat}")


if __name__ == "__main__":
    get_dict_norepeat()
    # get_list_norepeat_sorted()
    # get_list_norepeat()