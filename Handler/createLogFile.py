import logging
from pathlib import Path

def setupLog():
    logFile = Path ("./Handler/Log/testlog.log")
    logFile.parent.mkdir(parents=True,exist_ok=True)    #create folder log
    logFormat = "%(asctime)s:%(levelname)s-%(message)s"

    logging.basicConfig(level=logging.INFO,
                        filename=logFile,
                        format=logFormat,
                        filemode="w")

def testprintlog():
    print("bat dau test...")
    
    logging.info("test log info 1")
    logging.error("test log info 2")
    
if __name__ == "__main__":
    setupLog()
    testprintlog()

