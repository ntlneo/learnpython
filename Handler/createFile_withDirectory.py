
#Both 2 ways must create Directory 1st, then create File

#2: use pathlib     #new python 3.4
from pathlib import Path
filename = Path("./Handler/FileToTest2/testfile2.txt")
filename.parent.mkdir(exist_ok=True,parents=True)       #Create dir
filename.write_text("test 4")                           #Create file
print(filename.read_text())



# #1: use os lib    #old
# import os
# filename = "./Handler/FileToTest1/testfile1.txt"        #path begin from Home directory of project: ./          
# os.makedirs(os.path.dirname(filename), exist_ok=True)   #create folders if it doesn't exist
# with open(filename,"w+") as f:                          #open to write, create file if it doesn't exist
#     f.write("test write to file 5")
# print(open(filename).read())


