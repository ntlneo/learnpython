import re
"""
    sub (1_word_need_to_replace, 2_word_to_replace, string) : replace 1 by 2
    findall (pattern, string, [re.IGNORECASE])              : get list all words in string that match the pattern
    search (pattern, string, [re.IGNORECASE])               : get only 1 Match object when at least a word in string that match the pattern
    match (pattern, string, [re.IGNORECASE])                : get 1 Match object if pattern match the beginning of string
    The function re.finditer does the same thing as re.findall, except it returns an iterator, rather than a list.
    
    ***Metacharacter:
    .       : any character
    ^       : start
    $       : end
    |       : or
    -> "^gr.y$"         : has 4 char, start with 'gr', then follow with any character, except a newline, and end with 'y'.     ->grey, grzy
    -> "^m..e$"         : has 4 char, start with 'm', then follow with 2 any character, except a newline, and end with 'e'.    ->maae, mz1e
    -> "BaoAn|ThaoMy"   : has BaoAn or ThaoMy       ->s = "dsafdsaf BaoAn asdfsafsa ThaoMy asdfasdasd BaoAn sa ThaoMydasfds" > BaoAn, ThaoMy, BaoAn, ThaoMy
    
    ***Repeatition metacharacter:
    *       : 0+
    +       : 1+
    ?       : 0 or 1
    {1,4}   : 1 or 2 or 3 or 4. If the first number is missing, it is taken to be zero. If the second number is missing, it is taken to be infinity.      ->{0,1} = ?
    
    -> "egg(spam)*" : has 'egg', then has 'spam' or not.     ->egg, eggzz, eggspamzzz
    -> "g+"         : must has 'g'       ->gzz, gggzzz
    -> "colo(u)?r"  : has 'u' or not            ->color, colour
    -> "9{1,3}$"    : end with 1-3 number 9     ->9, z99, zzz999
    
    ***Character Classes:
    []      : match any character inside []
        - The class [a-z] matches any lowercase alphabetic character.
        - The class [G-P] matches any uppercase character from G to P.
        - The class [0-9] matches any digit.
        - Multiple ranges can be included in one class. For example, [A-Za-z] matches a letter of any case.
        - Place a ^ at the start of a character class to invert it. The metacharacter ^ has no meaning unless it is the first character in a class.
    -> "[aei]"              : has any one of the characters defined 'a', 'e', 'i'.  -> zalsb, zzeivv
    -> "[A-Z][A-Z][0-9]"    : has at least 3 characters as format: first and second are alphabet from A-Z, 3rd is digit  -> LS8adsad, aasdSE3
    -> "[^A-Z]"             : has a none-casesensitive    -> AbCdEfG123, this is all quiet
    
    ***Groups:
    ()              : group a string
        - A call of group(0) or group() returns the whole match.
        - A call of group(n), where n is greater than 0, returns the nth group from the left.
        - The method groups() returns all groups up from 1.
    (?P<name>...)   : Named groups - where name is the name of the group, and ... is the content.
    (?:...).        : Non-capturing groups - They are not accessible by the group method, so they can be added to an existing regular expression without breaking the numbering.
    \1, \99         : This matches the expression of the group of that number.
    -> "egg(spam)*"         : has 'egg', then has 'spam' or not     ->egg, zeggspamz
    -> "a(bc)(de)(f(g)h)i"  : has 'abcdefghi'
    -> "(?P<first>abc)(?:def)(ghi)" : has 'abcdefghi'             ->match.group("first) > abc         ->match.groups() > abc, ghi
    -> "(.+)\1"            : has any word, then repeat that word                 ->word word, ?! ?!

    ***Special Sequences:
    \d  -   \D      : digit                 -       not digit
    \s  -   \S      : whitespace            -       not whitespace
    \w  -   \W      : character(a-zA-Z0-9)  - not character
    \A              : beginning string
    \Z              : end string
    \b  -   \B      : boundary between word and non-word
    -> "(\D+\d)"    : has a not digit, then a digit     ->hi 99!
    
    
"""

#metacharacter:
pat = r"gr.y"       #it means: word starts with 'gr', then any char, then 'y'
if re.match(pat,"greyzz"):
    print("Match 1")

if re.match(pat,"gray"):
    print("Match 2")

if re.match(pat,"blue"):
    print("Match 3")


"""
#function:
pattern = r"spam"
if re.match(pattern, "eggspamsausagespam"):         #true if re.match() return anything except NONE
    print("Match")
else:
    print("No match")

if re.search(pattern, "eggspamsausagespam"):        #true if re.search() return anything except NONE
    print("Match")
else:
    print("No match")

print(re.findall(pattern, "eggspamsausagespam"))    #return a list of substring that match pattern
z = re.finditer(pattern, "eggspamsausagespam")
for i in z:
    print(i.group())                #show subtring that match pattern


#replace a word
string = "how are you hai. hallehaya"
newstring3 = re.sub("ha","ZZ",string)       #replace pattern 'ha' by 'ZZ'
print(newstring3)
string.startswith()

#search from the beginning string
string = "how are you hai. hallehaya"
pattern = r"are"
pattern2 = r"how"
res = re.match(pattern,string, re.I)        #search from start -> this return None since 'are' not the 1st match
res2 = re.match(pattern2, string, re.I)     #return Match object
print(res)
print(res2)

#get all word contain ain , not case-sensitive
txt = "The rain in SpAin alAINzx country"
x = re.findall(r"\w*ain\w*", txt, re.I)
print(x)
"""



"""
regex from google:    https://developers.google.com/edu/python/regular-expressions
"""

"""
stringgoogle = 'xyz alice-b@google.com purple monkey and lam.nguyenthanh84@gmail.com is great'

# #get list email in string
pattern_email = r"[\w.-]+@[\w.-]+" #[abc] = a or b or c -> [\w.-] = any word or . or -
list_email = re.findall(pattern_email, stringgoogle, re.I)
for email in list_email:
    print(email)

# #get list username and host of email
pattern_email_group = r"([\w.-]+)@([\w.-]+)"  #()() = group(1) group(2)
list_object_findall = re.findall(pattern_email_group, stringgoogle, re.I)
for object in list_object_findall:
    print(f"username = {object[0]}")
    print(f"host = {object[1]}")

#get only 1st username and host of email
list_object_search = re.search(pattern_email_group,stringgoogle,re.I)
print(list_object_search)
print(stringgoogle[4:22])
print(list_object_search.groups())
print(list_object_search.group())
print(f"username = {list_object_search.group(1)}")
print(f"host = {list_object_search.group(2)}")
"""