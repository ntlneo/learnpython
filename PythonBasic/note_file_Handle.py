

with open("//File/newfile.txt","w+") as f:
    print(f.read())


# file = open("books.txt", "r")
# #your code goes here
# lines = file.readlines()
# print(lines)
# for line in lines:
#     # word_without_n = line.removesuffix("\n")
#     newword = line.replace("\n","")
#     print(newword)    
#     code = list(newword)[0] + str(len(newword))
#     print(code)
# file.close()

# with open("newfile.txt") as f:
#     print(f.read())


# x = 2
# try:
#     file = open("newfile2.txt")      
#     print(file.read())
# # except:
# #     print(FileNotFoundError("Open file ko dc vi ko co file"))
# finally:
#         try:            
#             file.close()
#         except:
#             print(NameError("Close file ko dc vi bien file chua dc dinh nghia"))
# print("xu ly file xong")


# names = ["Nguyen", "John", "Oscar", "Jacob", "Lam"]
# file = open("names.txt", "w+")
# #write down the names into the file
# # for name in names:
# #     file.write(name + "\n")
# file.writelines([name+'\n' for name in names])
# file.close()
# file= open("names.txt", "r")
# #output the content of file in console
# for name in file.readlines():
#     print(name)
# file.close()

# msg = "Hello world!"
# file = open("newfile.txt", "w")
# amount_written = file.write(msg)
# print(amount_written==len(msg))
# file.close()




# file = open("newfile1.txt","w")
# file.close()    #open a file in write mode will delete the previous content

# file = open("newfile1.txt","r")   #default is r
# print(file.read())
# file.close()



# file = open("E:\\0. Hoc lap trinh\\Python_Practice\\test_read_file.txt","r+")
# content = file.read()   #read entire file, if read again will be empty
# print(len(content))
# print(content)
# file.close()

# file = open("E:\\0. Hoc lap trinh\\Python_Practice\\test_read_file.txt","r+")
# lines = file.readlines()    #return a list
# print(lines)
# for line in lines:
#     print(line)
# file.close()

