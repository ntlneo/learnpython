
#calculate the appearance time of char in a text file, char is none casesensitive
filename = input("Enter a filename: ")
with open(filename) as file:
    content = file.read()
# print(content)

#for none case sensitive
content = content.lower()
print(f">>>File content (none case-sensitive): \n{content}")

listchar = list(content)
print(f">>>Total word: {len(content)}")
print(len(listchar))
listchar_occur1time = []

for char in listchar:    
    if char not in listchar_occur1time and char.isalpha():
        listchar_occur1time.append(char)
print(f">>>List char in content: {listchar_occur1time}")

for char_occur1time in listchar_occur1time:
    occupation = content.count(char_occur1time)
    notround = occupation/len(content)
    percent = notround*100
    print(f"{char_occur1time} occurs {occupation} times = {round(percent,2)}%")
    # print(f"{char_occur1time} occurs {occupation} times = {percent:.2f}%")
    
