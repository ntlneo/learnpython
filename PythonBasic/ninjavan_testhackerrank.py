"""
Ninja Van list questions HackerRank:

Reverse A List

Longest Even Length Word

Selenium Java: Form Submission

Rest: Http Status Codes

Common Students

Json Selector

Json Data Type

Curl

"""


# Ninja Van - cau 1
def reverseArrayList(a):
    # Write your code here
    # lista = list(a)
    # lista.reverse()
    # return lista

    return a[::-1]

    # lista = []
    # for number in range(len(a)-1,-1,-1):
    #     lista.append(a[number])
    # return lista

    # iterator = reversed(a)
    # alist = list(iterator)
    # # alist = [*iterator]
    # return alist


arra = [5, 3, 1, 2, 7, "thanh", "lam"]
# arr = 0
print(f"orgin list: {arra}")
# rarr =
print(f"reversed list: {reverseArrayList(arra)}")


# Ninja Van - cau 2
def longestEvenWord(sentence):
    # Write your code here
    list_word = sentence.split()
    longest = 0
    longestword = "00"
    for word in list_word:
        if len(word) % 2 == 0 and len(word) > longest:
            longest = len(word)
            longestword = word
    return longestword


sentence = "It is a pleasant day today"
# sentence = "hey"
print(longestEvenWord(sentence))
