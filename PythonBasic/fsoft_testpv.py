"""
Cho mảng sau: [5,2,4,8,7,3,6,1,9,8,10]
1. Hãy viết function để có output như sau [2,4,6,8,8,10,9,7,5,3,1]
2. Tìm số lớn thứ 2 trong mảng trên
3. [Tự thêm] tìm số lớn nhất và số nhỏ nhất (ko xài hàm tiện lợi)
4. Sắp xếp nhỏ đến lớn và lớn đến nhỏ dùng vòng lặp (ko xài hàm sort)
"""

arr = [5,2,4,15,8,7,3,6,1,9,8,10]
#  #1 
# list1 = [sochan for sochan in arr if sochan % 2 == 0]
# list2 = [sole for sole in arr if sole % 2 != 0]
# list1.sort()
# list2.sort(reverse = True)
# print(list1 + list2)

# #2
# arr.sort(reverse=True)
# print(arr)
# for i in range(0,len(arr)):
#     if arr[i] != arr[i+1]:
#         max2nd = arr[i+1]
#         break   
# print(max2nd)

# #3
# maxz = arr[0]
# for i in range(0, len(arr)):
#     if arr[i] > maxz:
#         maxz = arr[i]
# print(maxz)
# print(max(arr))
    
# minz = arr[0]    
# for i in range(0, len(arr)):    
#     if arr[i] < minz:
#         minz = arr[i]
# print(minz)
# print(min(arr))

# #4
#tang dan
# arr.sort()
# print(arr)
for i in range(0,len(arr)-1):
    for j in range (i+1,len(arr)):
        if arr[i] > arr[j]:
            temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp           
print(arr)


#giam dan
# arr.sort(reverse = True)
# print(arr)
for i in range(0,len(arr)-1):
    for j in range (i+1,len(arr)):
        if arr[i] < arr[j]:
            temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp           
print(arr)

            
            
            
        
