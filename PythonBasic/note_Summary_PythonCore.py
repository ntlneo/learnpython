
#9 Major 3rd party libs
# BeautifulSoup : is very useful for scraping/crawling data from websites
# urllib        : to access website is so hard to use
# matpilot      : allows you to create graphs based on data in Python.
# NumPy         : allows for the use of multidimensional arrays. 
#                 It also contains functions to perform mathematical operations such as matrix transformations on the arrays.
# SciPy         : contains numerous extensions to the functionality of NumPy.
# Panda3D       : for create 3D game
# pygame        : for create 2D game


#8. __main__
#if import file, the "if __name__ == "__main__":" will not run
#if just run file, both print"Hello" and the "if __name__ == "__main__" will run
print("hello")
if __name__ == "__main__":
    print(f"name cua test_import.py la: {__name__}")


#7. ELSE with try-except
#if try run normal -> do else
#if except run -> don't do else
try:
    x = 2/4
    print(1)
except:
    print(2)
else:
    print(3)


#7. ELSE with loop (for,while)
#loop run normal (have no break) -> do else
#loop have break -> don't do else
for i in range(10):
    if i == 99:
        break
else:
    print("Do this else")
    

#6. Ternary Operator (toán tử bậc 3): apply for Tuple, dict, lambda
# TUPLE:    (if_test_false,if_test_true)[test]
a, b = 10, 20
print((b,a)[a > b])     # if [a<b] is true it return 1, so element with 1 index will print
                        # else if [a<b] is false it return 0, so element with 0 index will print

# DICT:     {True: value_true, False: value_false}[a<b]
print({True: a, False: b}[a<b])

# LAMBDA:   (lambda: b, lambda: a)[a<b]()   -> I see it the same tuple
print((lambda: b, lambda: a)[a < b]())


#5. Ternary Operator (toán tử bậc 3): apply for if else
# [on_true] if [expression] else [on_false] 
# expression : conditional_expression | lambda_expr
a = 7
b = 1 if a >= 5 else 42
print(b)


#4. Tuple/list Unpacking
#Unpacking allows you to assign each item in an iterable (often a tuple) to a variable.
numbers = [1,2,3,4,5]
a,*b,c = numbers
print(a)
print(b)
print(c)

numbers2 = (1, 2, 3, 'thanh', 'lam')        
a, b, c,d,e = numbers2              #FUN: use unpack to manually reverse a tuple/list
# a,b = b,a                           #or manually swap
reverse_numbers2 = (e,d,c,b,a)
print(reverse_numbers2)


#3. Function Arguments: **kwargs -> kwargs is dict
def my_func(x, y=7,*args, **kwargs):
    print(x+y)
    print(kwargs)
my_func(2,3,4,5,a=7,b=8)


#2. Function Arguments: *args -> args is tuple
#Python allows to have function with varying number of arguments 
def adder(*x):
    result = 0
    for num in x:
        result += num
    print(result)
adder(2, 3)
adder(1, 2, 3, 4, 5)
 

#1. Default Value: should no non-default follows a default
#if don't input value for var, it will get default value
def function(x, y, food="zzz"):
    print(food)
function(1, 2)
function(3, 4, "egg")


