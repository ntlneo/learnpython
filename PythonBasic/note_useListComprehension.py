#Syntax:  newlist = [expression FOR item IN iterable IF condition == True]
# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
# newlist = [x.upper() for x in fruits if "a" in x]
# print(newlist)

# #get odd or even from list digit
# listnum = list(range(11))
# print(listnum)
# listsochan = [sochan for sochan in listnum if sochan % 2 == 0]
# listsole = [sole for sole in listnum if sole % 2 != 0]
# print(listsochan)  
# print(listsole)

# #get string from list item
# strz = ["hello", 'how are', 'lam', 123, 567, "ali", 'ba']
# print(f"new string after list comprehension: {' '.join(str(x) for x in strz)}")

cubes = [i**3 for i in range(5)]
mytuple = *cubes,
print(cubes)
tuplecubes = tuple(cubes)
print(tuplecubes)
print(mytuple)

