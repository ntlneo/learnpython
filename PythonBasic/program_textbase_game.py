def get_input():  
  command = input(": ").split()
  verb_word = command[0].lower()
  if verb_word in verb_dict:
    func_verb = verb_dict[verb_word]
  else:
    print('Unknown command "{}"'. format(verb_word))
    return False

  if len(command) >= 2:
    noun_word = ' '.join(command[1:])
    print (">>>> " + func_verb(noun_word))
  else:
    print(func_verb("nothing"))


#define all class in Game
class GameObject:
  className = ''
  desc = ''
  objects = {}

  def __init__(self,name) -> None:
    self.name = name
    GameObject.objects[self.className] = self
    
    
  def get_desc(self):
    return 'There\'s a ' + self.className + ". It's " + self.desc
 
class Goblin(GameObject):
  def __init__(self, name) -> None:
    self.className = "goblin"         #to override className in GameObject. This is key of GameObject.objects
    self._desc = "A FOUL CREATURE"    #to override desc in GameObject
    self.health = 3
    super().__init__(name)
    
  @property
  def desc(self):
    if self.health >= 3:
      return self._desc
    elif self.health == 2:
      health_line = "It has a wound on its knee." + f"\n{self.className}'s health = {self.health}"
    elif self.health == 1:
      health_line = "Its left arm has been cut off!" + f"\n{self.className}'s health = {self.health}"
    elif self.health <= 0:
      health_line = "It is dead." + f"\n{self.className}'s health = {self.health}"
    
    return self._desc + "\n" + health_line
  
  @desc.setter
  def desc(self, value):
    self._desc = value
  
  

  
#define func_word
def func_say(noun):
  return 'You said "{}"'.format(noun)

def func_run(noun):
  return f'You ran "{noun}"'

def func_find(noun):
  return f'You found "{noun}"'

def func_get(noun):
  return f'You got "{noun}"'

def func_test(noun):
  noun_lower = noun.lower()
  if noun_lower in GameObject.objects:
    return GameObject.objects[noun_lower].get_desc()
  else:
    return f'There\'s no "{noun}" here'
  
def func_hit(noun):
    if noun in GameObject.objects:
      thing = GameObject.objects[noun]
      if type(thing) == Goblin:
        thing.health = thing.health - 1
      if thing.health <= 0:
        msg = "You killed the goblin" + f"\n {thing.className}'s health = {thing.health}"
      else:
        msg = "You hit the {}".format(thing.className) + f"\n{thing.className}'s health = {thing.health}"
    else:
      msg = f"There's no {noun} here"
    return msg 
  
#insert verb and func_verb
verb_dict = {
  "say": func_say,
  "run": func_run,
  "find": func_find,
  "get": func_get,
  "test": func_test,
  "hit": func_hit
}      

#create monster
goblin1 = Goblin("Lam_la_goblin")
# print(goblin.name)
print(GameObject.objects)
# print(verb_dict)

print(f"======== GAME START ========")
commands_in_dict = [key for key in verb_dict.keys()]
print(f"Please enter one of commands: {commands_in_dict}")
  
while True:  
  if get_input() == False:
    print("========= GAME END =========")
    break


  



