"""
unpacking operator: 
*args : * can be used on any iterable
**kwarg : ** can only be used on dictionaries

the correct order for your parameters is:
Standard arguments
*args arguments
**kwargs arguments
"""
def sumnum(*args):
    return sum(args)
print(sumnum(1,3,4,5,6))

#concatenate_keys.py
def concat(**kwargs):
    result = ""
    # Iterating over the KEYS of the Python kwargs dictionary
    for arg in kwargs:
        result += arg
    return result
print(concat(a="Real", b="Python", c="Is", d="Great", e="!"))

#concatenate_2.py
def concatenate(**words):
    result = ""
    # Iterating over the VALUES of the Python kwargs dictionary
    for arg in words.values():
        result += arg
    return result
print(concatenate(a="Real", b="Python", c="Is", d="Great", e="!"))

"""
When you use the * operator to unpack a list and pass arguments to a function, 
it's exactly as though you're passing every single argument alone. 
This means that you can use multiple unpacking operators to get values from several lists and pass them all to a single function.
"""
def my_sum(*args):
    result = 0
    for x in args:
        result += x
    return result
list1 = [1, 2, 3]
list2 = [4, 5]
list3 = [6, 4, 1, 9]
print(my_sum(*list1, *list2, *list3))


"""
In this example, my_list contains 6 items. 
The first variable is assigned to a, the 2nd to c, the last to d and all other values are packed into a new list c. 
"""
# get sub list from list
my_list = [1, 2, 3, 4, 5, 6]
a, b, *c, d =  my_list  #c will remove 1,2 and last item
t,*z,x = my_list    #z will remove 1st and  last item from list
print(a)
print(b)
print(c)
print(d)
print(z)

# get char from string
a = "RealPython"
print(*a)

# get word from string , phan cách = chữ hoa
a = "RealPythonIsGood"        #expected = Real Python Is Good
listchar = [*a]
print(listchar)
indexchar = []
for index in range(len(listchar)):      #get list index of capital chars
    if index != 0 and listchar[index].isupper():
        indexchar.append(index)        
print(indexchar)
count = 0                               #insert space before capital chars
for num in indexchar:
    listchar.insert(num+count," ")
    count += 1
print(listchar)
newstring = ''.join(listchar)            #get string and list with every capital char
print(newstring)
listCapital = newstring.split()
print(listCapital)

# get list char from string
print([*a])     #string to list char
print(list(a))  #string to list char
print(''.join([*a]))    #list char to string
