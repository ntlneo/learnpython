
squares = {1: 'c', 2: 4, 3: "error", 4: 16}
squares[8] = 99         #nếu ko co index đó thì tuong duong voi update:    squares.update({8: 99})
squares.update({6: 66})    
squares[3] = 10
print(squares)
print(squares.get(55,"gia tri default"))      #if can't get value will show "gia tri default"
if 1 not in squares:    #key only, tuong duong voi square.keys()
    print("pass")
else:
    print("fail")    
if 1 in squares.values():    #value only
    print("pass")
else:
    print("fail")
    

family = {"lam":40, "thanh":30, "an":12}
print(family["lam"])    #tuong duong voi lenh get:  print(family.get("lam"))
print(family["an"])
print(family.keys())
print(family.values())
for k in family.keys():
    print(k +": " + str(family[k]))
for item in family.items():
    # print(item)
    for v in item:
        print(v)
        

bad_dic = {
    (1,2,3): "one two three"
}
print(bad_dic.get((1,2,3)))