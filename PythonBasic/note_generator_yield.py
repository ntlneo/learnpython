"""
    Generator: in a function, use to return a iterable (insteaf of return a value then add this value to an iterator such as a list)
    keyword:        yield var --> return var then add var to an iterator
    access Generator: use for loop or convert it to list,tuple ...
    
"""

def countdown():
    i=5
    while i > 0:
        yield i+10
        i -= 1
r = []        
for i in countdown():
    r.append(i)
print(r)
        
names = ['Lam', 'Thanh', 'An', 'My']
def getListEvenString():
    for name in names:
        if len(name) % 2 == 0:
            yield name            
print(list(getListEvenString()))
    
listname = list(filter(lambda name: len(name) % 2 == 0, names))
print(listname)

