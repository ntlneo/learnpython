print(", ".join(["spam", "eggs", "ham"]))
print("Hello ME".replace("ME", "world"))
print("This is a sentence.".startswith("This"))
print("This is a sentence.".endswith("ce."))
print("This is a sentence.".upper())
print("AN ALL CAPS SENTENCE".lower())
print("spam, eggs, ham".split(", "))

print(min(1, 2, 3, 4, 0, 2, 1))
print(max([1, 4, 9, 2, 5, 6, 8]))
print(abs(-99))
print(abs(42))
print(sum([1, 2, 3, 4, 5]))
print(round(3.869,2))
print(round(3.434))

nums = [55, 44, 33, 22, 11]        
if all([i > 5 for i in nums]):
    print("All larger than 5")
if any([i % 2 == 0 for i in nums]):
    print("At least one is even")
for tuple_item in enumerate(nums):
    print(tuple_item)
for tuple_item in enumerate(nums,3):
    print(f"tuple1 = {tuple_item[0]} and tuple2 = {tuple_item[1]}")
for k,v in enumerate(nums,5):
    print(f"keys is {k}, value is {v}")
    
