"""
Chúng ta thường sử dụng lambda function khi mà cần một hàm không tên trong một khoảng thời gian ngắn.
Lambda function thường được sử dụng cùng với các hàm tích hợp sẵn như filter(), map(), reduce()... or goi 1 ham trong 1 ham khac

Syntax:     lambda arguments: expression
"""
def my_func(f, arg):
  return f(arg)
print(my_func(lambda x: 2*x*x, 5))

print((lambda x: x+10)(3))

result = lambda x,y: x+y
print(result(3,5))

s = lambda x: x**2
print(s(5))

z = lambda x: sum(range(x+1))   #tinh tong tu 0 den x
print(z(4))

#lambda FILTER vs list comprehension: apply function with every items return list items match with predicate (condition of function is TRUE)
mylist = [1, 5, 4, 6, 8, 11, 3, 12]
listsole = list(filter(lambda x: x%2, mylist))
listsochan1 = list(filter(lambda t: t%2 == 0, mylist)) #lambda
listsochan2 = [x for x in mylist if x % 2 == 0]   #list comprehension
print(listsole)
print(listsochan1)
print(listsochan2)



#lambda MAP : apply function with every items return list results of function 
mylist2 = [1, 5, 4, 6, 8, 11, 3, 12]
newlist = list(map(lambda x: x * 2, mylist2))
newlist1 = list(map(lambda x: x%2, mylist2))
newlist2 = list(map(lambda x: x%2 == 0, mylist2))
print(newlist)
print(newlist1)
print(newlist2)



#lambda REDUCE : apply function with every 2 items of list until end --> return the last result 
from functools import reduce
mylist3 = [47,11,42,102,13]
#get max value
z = reduce(lambda x,y: x if x >= y else y, mylist3)
print(z)

#tinh tong, or giai thua tu 1 den 4
mylist4 = range(1,5)
tong = reduce(lambda x,y: x+y,mylist4)
print(tong)
giaithua = reduce(lambda x,y: x*y,mylist4)
print(giaithua)
