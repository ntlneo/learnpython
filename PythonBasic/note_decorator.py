"""
    Decor PATTERN meeans parent function return a sub function (wrap function) inside it
    inside wrap function, we handle code
    
    Decorators provide a way to modify functions using other functions.
    This is ideal when you need to extend the functionality of functions that you don't want to touch the old code
    
    after predefine a decorator using a wrap, we can use @[name of decor function] -> @decorat
    1 function can have MANY decorator
"""
#real decord 1: after decord, show === hello lam ====
def decorat(func):
    def wrap(s):
        print("=========")
        func(s)
        print("=========")
    return wrap
def new_func(strz):
    print(strz)
s = "Hello"
new_func = decorat(new_func)
new_func(s)      #tuong duong goi wrap(s)
@decorat
def show_string(s):
    print("Hello " +s)
show_string("lam")

#real decord 2: after decord, show uppercase
text = input()
def uppercase_decorator(func):
    def wrapper(text):
        #your code goes here
        return func(text).upper()
    return wrapper    
@uppercase_decorator    
def display_text(text):
    return(text)    
print(display_text(text))


#not decord
def tinhtong(x,y):
    def wrap(z):
        print(x+y+z)
    return wrap 
z = tinhtong(5,2)
z(1)


#not decord when contains 2 functions
def decord_by_multi(getx,gety):
    def wrap(num):
        print(f"Sau decord, ham getx = {getx(num)* 2}")
        print(f"Sau decord, ham gety = {gety(num)* 3}")
    return wrap

def getx(num):
    return num+1

def gety(num):
    return num+2

num = 2
print(f"ham getx ban dau = {getx(num)}")
print(f"ham gety ban dau = {gety(num)}")
afterdecord = decord_by_multi(getx,gety)
afterdecord(num)