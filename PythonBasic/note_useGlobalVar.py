# shot and calculate point
c = 1
p = 100

def shoot():
    # current_point = p
    global c,p
    if c != 1:
        print(f"The current point is: {p}")

    action = input(f"Shoot #{c} (Please enter \'hit\' or \'anything\'): ")

    if "hit" in action:
        p += 10
    else:
        p -= 20

while c <= 4:
    
    if c == 1:
        print(f"The initial point is: {p}")
        print(f"--- Start shoot ---")

    # p = shoot()
    shoot()
    c += 1
    
    if c == 5:
        print(f"The current point is: {p}")
        print(f"--- End shoot ---")

print(f"The final point is: {p}")

