
"""
There are three main types of modules in Python,
those you write yourself, those you install from external sources, and those that are preinstalled with Python.

The last type is called the standard library, and contains many useful modules. 
Some of the standard library's useful modules include string, re, datetime, math, random, os, 
multiprocessing, subprocess, socket, email, json, doctest, unittest, pdb, argparse and sys.

Tasks that can be done by the standard library include string parsing, data serialization, testing,
debugging and manipulating dates, emails, command line arguments, and much more!    
"""



import random
x = 10
ls = ['lam', 'thanh', 3, 1]
for i in range(len(ls)):
    value = random.choice(ls)
    print(value)


import math    
print(math.sqrt(10))


from math import pi,sqrt
print(pi)
print(sqrt(81))