
from itertools import accumulate, takewhile, chain
from itertools import count,cycle,repeat
from itertools import product, permutations

letters = ("A", "B", 'C')
nums = [1, 4]
lettter2 = ["A", "B", 'C', 'D']
#product : apply với nhiều list, cho ra các cách kết hợp ko trùng nhau của từng phần tử list này với từng phần tử list khác
print(list(product(letters, nums)))     #socach = 3 x 2 = 6 = len(list)  

#permutation : apply voi 1 list, cho ra các cách kết hợp ko trùng nhau giữa các phần tử trong list
print(list(permutations(lettter2)))     #ko điền r thì default r = len(letter2)
print(list(permutations(lettter2,3)))   #r = 3 nghĩa là tạo ra chỉnh hợp có 3 phần tử từ 4 phần tử--> socach = 4! / (4-3)! = 24 = len(list)

if True == 1:
    print(1)
else:
    print(2)
"""

mylist = [1,3,9,6,10,5]
mylist2 = ['lam', 'thanh', 'an']
mylist3 = [2,4]

#takewhile: lay tung phan tu cua list so với function dieu kien, nếu TRUE thi lấy tiếp, nếu FALSE thì dừng liền. cuối cùng show lsit ket quả . Cách hoat động gần giống FILTER
z = list(takewhile(lambda x: x <=6, mylist))    #takewhile dừng ngay lập tức khi FALSE
t = list(filter(lambda x: x <=6, mylist))       #FILTER  gặp FALSE vẫn tiếp tục cho đến khi hết list
print(z)
print(t)

#accumulate: lấy từng 2 phan tử của list, cho vào function rồi show list ket quả. Cách hoat động gần giống MAP
a = list(accumulate(mylist))                    #cộng gộp, default ko co function thì là sum gộp của x+y
b = list(accumulate(mylist,lambda x,y: x*y))    #có function thì show ket quả gộp của function
c = list(map(lambda x,y: x*y, mylist,mylist3))  #MAP cần 2 list, accumulate chi cần 1 list
print(a)
print(b)
print(c)

#chain: lấy tât cả phần tử của các list. cuối cùng show ra 1 list tổng
d = list(chain(mylist,mylist2))
print(d)




#count(3,2): bat dau đếm vô hạn lần từ 3 trở đi, step = +2  -> Same result with range
for i in count(3,2):    
    if i > 12:
        break
    else:
        print(i)
for i in range(3,13,2):
    print(i)

#cycle(list): duyet qua tat ca phan tu cua list, sau do lap lai tiep
list = [1,'thanh',10, 'an']
c = 0        
for i in cycle(list):    
    c += 1   
    if c > len(list)*4:     #print tat ca phan tu cua list 4 lan
        break
    else:
        print(i)
    
#repeat(list,4): duyet qua 1 list 4 lần -> tao ra 4 list giong nhau
for i in repeat(list,4):    #print list 4 lan
    print(i)

"""