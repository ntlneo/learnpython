import Codility_demotest_smallestPositiveNum as cod



# A = [1, 3, 6, 4, 1, 2] #>5
D = [1, 2, 3] #->4
# A = [1, 2, 6] #->4
A = [5,6,7] #->1
# A = [-1, -3] #->1
# A = [-1, 6, -3] #->1
C = [1, 3, 2, 4, 1, 6] #>5
B = [6, 3, 2, 4, 1, 1] #>5
# B = [1, 7] #->2
# A = [4, 7] #->1
# A = [3] #->1
# A = [-1000000,-9] #->1

def test_pass():
    assert True

def test_fail():
    assert False
    
# if __name__ == '__main__':
    