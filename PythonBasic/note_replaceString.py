import re
string = "how are you hai. halleluya"
string.lower()

#use string.replace(), replace all 'ha'  =  'AKKA'
newstring1 = string.replace("ha","AKKA",1)  #just repalce 1 time
newstring2 = string.replace("ha","AKKA")    #replace all times
print(newstring1)
print(newstring2)

#use regular expression, replace all 'ha'  =  'AKKA'
newstring3 = re.sub("ha","AKKA",string)
print(newstring3)

#replace all 'ain' = HAHA, none casesensitive
txt = "The rain in SpAin alAINzx country"
#get all words contain ain
list_expected_word = re.findall(r"\w*ain\w*", txt, re.I)
print(list_expected_word)
print(txt)
#replace in-place word in string
for word in list_expected_word:
    txt = txt.replace(word, "HAHA")
print(txt)

#replace 1st letter to capitalize letter
sample = "this is a sample string"
print(sample.title())


