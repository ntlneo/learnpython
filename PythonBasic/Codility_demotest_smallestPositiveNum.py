"""
Write a function:

def solution(A)

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [-1, -3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [-1,000,000..1,000,000].
"""


def solution(A):
    # Implement your solution here
    base_num = 1
    smallest_positive_integer = 0  
    nums_not_in_list = [memMax for memMax in range(base_num,max(A)+1)if memMax not in A]
    if nums_not_in_list != []:
        for num in nums_not_in_list:
            if num < min(A):
                smallest_positive_integer = min(nums_not_in_list)
            else:
                smallest_positive_integer = num
                break
    elif min(A) <= 0:
        smallest_positive_integer = 1    
    else:
        smallest_positive_integer = max(A) + 1       
    return smallest_positive_integer 
        

# A = [1, 3, 6, 4, 1, 2] #>5
# A = [1, 2, 3] #->4
# A = [1, 2, 6] #->4
# A = [5,6,7] #->1
# A = [-1, -3] #->1
# A = [-1, 6, -3] #->1
# A = [1, 3, 2, 4, 1, 6] #>5
# A = [6, 3, 2, 4, 1, 1] #>5
# A = [1, 7] #->2
# A = [4, 7] #->1
# A = [3] #->1
A = [-1000000,-9]
# print(solution(A))


            
    