

# get word from string , phan cách = chữ hoa capitalize
newstring = a
for char in a:
    if char.isupper():
        index = a.index(char)
        # print(char)
        newstring = newstring.replace(char,"*"+char)
print(newstring)
listword = newstring.split("*")
# for word in listword:
#     if word != "":
#         print(word)
ite = [word for word in listword if word != ""]
print(ite)


"""
#Create a Map of List of Functions
def map_function(x,y, list_function):
    return [function(x,y) for function in list_function]

def cong(x,y):
    return y + x

def nhan(x,y):
    return y * x

list_func = [cong, nhan]

print(map_function(2,10,list_func))
 

#lambda filter vs list comprehension -> the same result
mylist = [1, 5, 4, 6, 8, 11, 3, 12]
myfilter = list(filter(lambda t: t%2 == 0, mylist)) #lambda
myfilter2 = [x for x in mylist if x % 2 == 0]   #list comprehension
print(myfilter)
print(myfilter2)



#find longest string in list, use max() with len
lsstr = ['nguyenthanhthaomy','lam', 'nguyen', "thanha", "thanh bao an"]
lsint = [123, 456, 999, 678, 1234, 555, 6666]
print(max(lsstr, key = len))
print(max(lsint))
print(max(lsstr,lsint, key = len))

#unpacking operator *args: unpack a tuple, **kwarg unpack a dictionary
#the correct order for your parameters is:
#Standard arguments
#*args arguments
#**kwargs arguments
def sumnum(*args):
    return sum(args)
print(sumnum(1,3,4,5,6))

#concatenate_keys.py
def concatenate(**kwargs):
    result = ""
    # Iterating over the keys of the Python kwargs dictionary
    for arg in kwargs:
        result += arg
    return result
print(concatenate(a="Real", b="Python", c="Is", d="Great", e="!"))

#concatenate_2.py
def concatenate(**words):
    result = ""
    for arg in words.values():
        result += arg
    return result
print(concatenate(a="Real", b="Python", c="Is", d="Great", e="!"))

#string to list char , list char to string
s = "hello my world"
listchar = list(s)     #oruse listchar = [*s]
print(listchar)
news = ''.join(listchar)
print(news)

# list to string
ls = ['L','A','M', 'NGUYEN THANH', 'XX YY ZZ', 33, 44]
z = ''.join([str(x) for x in ls] )
mystring = ''
for x in ls:
    mystring += str(x)
print(mystring)
print(z)



strz = ["hello", 'how are', 'lam', 123, 567, "ali", 'ba']
print(f"origin list is: {strz}")

#error do int ko thể join thanh string, phải đổi int sang str truoc
print(f"new string after join: {''.join(strz)}") 

#replace int item in list by string: using list comprehension
print(f"new string after list comprehension: {' '.join(str(x) for x in strz)}")

#replace int item in list by string : using index
for o in strz:
    if type(o) == int:
        inx = strz.index(o)
        strz[inx] = str(o)
print(f"new string after replace index: {' '.join(strz)}")

#get list str/int from list
subliststr = [x for x in strz if type(x) is str]
sublistint = [x for x in strz if type(x) is int]


#replace h = xx in-place orrigin list , then print new list
# for item in strz:
#     word = list(item)    
#     if 'h' in word:
#         indexletter = word.index('h')
#         indexword = strz.index(item)
#         word[indexletter] = 'xx'
#         newword = ''.join(list(word))
#         print(newword)
#     strz[indexword] = newword        
# print(strz)

#replace h = xx then save as a new list , then print new list
# def replace_h_to_xx(listchar:list,old:str,new:str):
#     for char in listchar:
#         if char == old:
#             inx = listchar.index(char)
#             listchar[inx] = new
#             newstr = ''.join(listchar)
#             return newstr
#         else:
#             return ''.join(listchar)
# newls = []
# for item in strz:    
#     newls.append(replace_h_to_xx(list(item),'h','CCCC'))
# print(newls)



# sstr = ' '.join(strz)
# print(sstr.upper())
# print(sstr.startswith("ho"))
# lstr = sstr.split()
# print(lstr)



#reverse a string
str = input()
print(str[::-1])

# x = list(str)
# x.reverse()
# z = ""
# for char in x:
#     z += char
# print(z)


#reverse a list
arra = [5, 3, 1, 2, 7, "thanh", "lam"]
print(arra[::-1])

    # lista = list(a)
    # lista.reverse()
    # print(lista)



    # lista = []
    # for number in range(len(a)-1,-1,-1):
    #     lista.append(a[number])
    # print(lista)

    # iterator = reversed(a)
    # lista = list(iterator)
    # lista = [*iterator]
    # print(alist)

# print(f"orgin list: {arra}")
# print(f"reversed list: {lista}")


#calculate price for cart
cart = [15, 42, 120, 9, 5, 380]

discount = int(input())
total = 0

for item in cart:
    price1item_afterDicount = item - (item*discount/100)
    total += price1item_afterDicount

print(total)



#not in list
nums = [1, 2, 3]
print(4 not in nums)
print(4 not in nums)
print(not 3 in nums)
print(3 not in nums)



#List comprehension
#Syntax:  newlist = [expression for item in iterable if condition == True]
fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
newlist = [x.upper() for x in fruits if "a" in x]
print(newlist)

listnum = list(range(11))
print(listnum)
listsochan = [sochan for sochan in listnum if sochan % 2 == 0]
listsole = [sole for sole in listnum if sole % 2 != 0]
print(listsochan)  
print(listsole)


#String to char
str = "   Hello world    "
ssplit = str.split()
sstrip = str.strip()
print(ssplit)
print(sstrip)

listchar = list(sstrip)
listchar2 = [*sstrip]
print([char for char in sstrip])
print(listchar)
print(listchar2)



#list add, extend, +, *
nums = [1, 2, 3]
print(f"Origin nums: {nums}")
nums.extend([4,5,6])
print(f"Extended many items nums: {nums}")
nums.extend(nums)
print(f"Extended itself nums: {nums}")
nums + ['a', 'b', 'c']
print(f"+ nums: {nums}")
nums * 2
print(f"* nums: {nums}")
print(nums[:])
print(nums[:14])

#list change value, min,max,range
nums = [10, 9, 8, 7, 6, 5]
nums[0] = nums[1] - 5
nums += [44, 33]
nums1 = nums + [66]
nums2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11]
nums3 =[99]
print(nums)
print(max(nums))
print(max(nums,nums1,nums2, nums3))
print(list(range(10)))

#slicing rever list
nums = [1, 2, 3, 4, 5, 6, 7]
rever = nums[::-1]
print(rever)

#slicing so chan reverse
part = nums[-2::-2]
print(part)


#Calculate price 5 passengers based on age
total = 0
count = 1
# print(f"Count = {count}")
valid = 0
while True:    
    if count <= 5:
        print(f"Age of customer {count}")
        age = int(input())
        count += 1
        if age >= 3:           
            total += 100
            valid += 1               
            print(f"Total price = {total}")
            print(f"Valid customer = {valid}")
        else:
            continue    
    else:
        break

print(total) 



#Shot and calculate point
count = 1
point = 100

def shoot(p,c):
    current_point = p
    if c != 1:
        print(f"The current point is: {current_point}")

    action = input(f"Shoot #{c} (Please enter \'hit\' or \'anything\'): ")

    if "hit" in action:
        current_point += 10
    else:
        current_point -= 20
    
    return current_point


while count <= 4:
    
    if count == 1:
        print(f"The initial point is: {point}")
        print(f"--- Start shoot ---")

    point = shoot(point,count)
    count += 1
    
    if count == 5:
        print(f"The current point is: {point}")
        print(f"--- End shoot ---")

print(f"The final point is: {point}")

#root
x = 81**(1/2)
print(x)


#calculate hour and min of 888 mins
min_start = 888
hour = min_start // 60
min_remain = min_start % 60
print(hour)
print(min_remain)

#calculate flight time in hours
distance = 7425
speed = 550
hour = distance / speed
print(hour)

#join
alist = ["lam", "bao an"]
string = " DiemDanh "
string_after = string.join(alist)
print(string_after)

#range
#listnum = set(range(1,101))
chuoisochan = list(range(0,11,2))
print(chuoisochan)
print(len(chuoisochan))
"""
