
"""
Python Collections (Arrays)
There are four collection data types in the Python programming language:

    List is a collection which is ordered and changeable. Allows duplicate members.             -> insert, append, reverse ...
    Tuple is a collection which is ordered and UNCHANGEABLE. Allows duplicate members.          -> only count, index
    Set is a collection which is UNORDERED, unchangeable*, and unindexed. NO DUPLICATE members. -> add, discard, update ..., if dup, only count 1
    Dictionary is a collection which is ordered** and changeable. NO DUPLICATE members.         -> key, value ... if dup, get the last
    
    When to use a dictionary:
    - When you need a logical association between a key:value pair.
    - When you need fast lookup for your data, based on a custom key.
    - When your data is being constantly modified. Remember, dictionaries are mutable.

    When to use the other types:
    - Use lists if you have a collection of data that does not need random access. Try to choose lists when you need a simple, iterable collection that is modified frequently.
    - Use a set if you need uniqueness for the elements.
    - Use tuples when your data cannot change.
    
    Note: Many times, a tuple is used in combination with a dictionary, for example, a tuple might represent a key, because it's immutable.
"""

###collections
mylist = [1, 5, "thanh", "an", 1, True, 8]
mytuple = (3, "lam", False, 9, 'lam')
myset = {'my', 10, True, 11, 10}
mydict = {'a':'20', '99': 'h', 'z': True, 'a':10}
print(mylist)
print(mytuple)
print(myset)
print(mydict)





###SET
first = {1, 2, 3, 4, 5, 6}
second = {4, 5, 6, 7, 8, 9}

print(first | second)   #lay tat ca o 2 set
print(first & second)   #lay chi phan chung
print(first - second)   #lay chi rieng o 1
print(second - first)   #lay chi rieng o 2
print(first ^ second)   #lay rieng 1 va rieng 2, ko lay phan chung
 
# set_letter = {'a','b','c'}
# if 'e' not in set_letter:
#     print(1)
# else:
#     print(2)
# print(set_letter)
# set_letter.pop()
# print(set_letter)
# set_letter.add('z')
# print(set_letter)
# set_letter.remove('z')
# print(set_letter)