import Codility_demotest_smallestPositiveNum as cod
import unittest


# A = [1, 3, 6, 4, 1, 2] #>5
D = [1, 2, 3] #->4
# A = [1, 2, 6] #->4
A = [5,6,7] #->1
# A = [-1, -3] #->1
# A = [-1, 6, -3] #->1
C = [1, 3, 2, 4, 1, 6] #>5
B = [6, 3, 2, 4, 1, 1] #>5
# B = [1, 7] #->2
# A = [4, 7] #->1
# A = [3] #->1
# A = [-1000000,-9] #->1

class Test_demoCodility(unittest.TestCase):
    def test_solution1(self):       
        self.assertEqual(cod.solution(A),1,"ket qua ko = 1")
        
    def test_solution2(self):
        self.assertEqual(cod.solution(B),2,"ket qua ko = 2")
        
    def test_solution3(self):
        self.assertEqual(cod.solution(C),5,"ket qua ko = 5")
        
    def test_solution4(self):
        self.assertEqual(cod.solution(D),5,"ket qua ko = 5")

if __name__ == '__main__':
    unittest.main(verbosity=2)