

"""
Properties:   @property, method must have SELF
- When the instance attribute with the same name as the method is accessed, the method will be called instead.
- One common use of a property is to make an attribute read-only.
- Property/setter/getter : just need call Name when using

property: @property             -> call: object.NamePropertyMethod
setter: @NameProperty.setter    -> call: object.NamesetterMethod
getter: @NameProperty.getter    -> call: object.NamegetterMethod  

"""

class Pizza:
    def __init__(self, toppings):
        self.toppings = toppings
        self.bo = False

    @property
    def pineapple_allowed(self):
        return self.bo
      
    @pineapple_allowed.setter
    def pineapple_allowed_set(self,value):
      self.bo = value
    
    @pineapple_allowed.getter
    def pineapple_allowed_get(self):
      return self.bo

pizza = Pizza(["cheese", "tomato"])
print(pizza.pineapple_allowed)        #call property
# pizza.pineapple_allowed = True    #error since property is read-only
pizza.pineapple_allowed_set = True    #call setter
print(pizza.pineapple_allowed_get)    #call getter


"""
Static Method: @staticmethod
- call direct: ClassName.staticmethodName()
- Static methods can be called without creating an object of the class.
- Các phương thức tĩnh hoạt động giống như các hàm đơn giản (ko cần self), ngoại trừ thực tế là bạn có thể gọi chúng từ một thể hiện của lớp.
"""

"""
class Calculator:
    #your code goes here
    @staticmethod
    def add(a,b):
        return a+b        
n1 = int(input())
n2 = int(input())
print(Calculator.add(n1, n2))       #call staticmethod direct by ClassName
c = Calculator()
print(c.add(n1,n2))                 #call staticmethod by instance

class Pizza:
  def __init__(self,topping) -> None:
    self.topping = topping
  
  @staticmethod
  def valid_topping(topping):
    if topping == "pinapple":
      raise ValueError("No service pinaaple")
    else:
      return True

listtopping = ['aplle', 'cherry', 'orange']
# if all([Pizza.valid_topping(i) for i in listtopping]):
if all(list(filter(lambda x: Pizza.valid_topping(x), listtopping))):
  newpizza = Pizza(listtopping)
  print(newpizza.topping)

"""

"""
Class Method: @classmethod, must have cls
- cls is same as self, it could be any name. 
- But cls uses for class, self uses for instance
"""

"""
class Rectangle:
  def __init__(self,dai,rong) -> None:
    self.dai = dai
    self.rong = rong
  
  def area(self):
    return self.dai * self.rong
  
  @classmethod
  def newsquare(cls,side_length):
    return cls(side_length,side_length)
  
a = Rectangle(3,5)
print(a.area())
z = a.newsquare(3)
print(z.area())
b = Rectangle.newsquare(5)
print(b.area())
"""

"""
#Private :  begin with __var. Object external can acc private by _ClassName__var
#Basically, Python protects those members by internally changing the name to include the class name.
class Spam:
    __egg = 7                 #private var __egg
    def print_egg(self):      
        print(self.__egg)

s = Spam()
s.print_egg()
print(s._Spam__egg)            #external object s access private by _Spam__egg
print(s.__egg)                 #error, no attribute __egg
"""

"""
Magic method:  (__init__ : for create instance of a class)
- Are special methods which have double underscores at the beginning and end of their names. They are also known as dunders.
- One common use of them is operator overloading: This means defining operators for custom classes that allow operators such as + and * to be used on them.

Magic method for operator:
__add__ for +         # x + y -> x.__add__(y)
__sub__ for -
__mul__ for *
__truediv__ for /
__floordiv__ for //
__mod__ for %
__pow__ for **
__and__ for &
__xor__ for ^
__or__ for |

Python also provides magic methods for comparisons:
__lt__ for <
__le__ for <=
__eq__ for ==
__ne__ for !=
__gt__ for >
__ge__ for >=

There are several magic methods for making classes act like containers.
__len__ for len()
__getitem__ for indexing
__setitem__ for assigning to indexed values
__delitem__ for deleting indexed values
__iter__ for iteration over objects (e.g., in for loops)
__contains__ for in
"""

"""

#magic method for CONTAINER
import random
class VagueList:
    def __init__(self, content):
        self.content = content

    def __getitem__(self, index):                     #for list[index]
        return self.content[index + random.randint(-1, 1)]

    def __len__(self):                                #for len(list)
        return random.randint(0, len(self.content)*2)
      
    def __setitem__(self,index, value):               #for list[index] = value
        index_decrease1 = index-1
        doublevalue = value*2
        self.content[index_decrease1] = random.randint(0,doublevalue)
        print(self.content[index_decrease1])
        
vague_list = VagueList(["A", "B", "C", "D", "E"])
print(vague_list.__len__())
print(len(vague_list))
print(vague_list.__getitem__(2))
print(vague_list[2])
vague_list[3]=4
print(vague_list.content)
"""

"""
#magic method for OPERATOR
class Vector2D:
  def __init__(self, x, y):
    self.x = x
    self.y = y
    
  def __add__(self, other):
    return Vector2D(self.x + other.x, self.y + other.y)
  
  def __sub__(self, other):
    return Vector2D(self.x - other.x, self.y - other.y)

first = Vector2D(5, 7)
second = Vector2D(3, 9)

sum2d = first + second      #tuong duong voi sum2d = first.__add__(second) = Vector2D(first.x + second.x , first.y + second.y)
print(sum2d.x)
print(sum2d.y)

minus2d = first - second
print(minus2d.x)
print(minus2d.y)

"""



"""

#inheritance 2 level
class A:
  def method(self):
    print("A method")

class B(A):
  def second_method(self):
    print("B method")

class C(B):
  def third_method(self):
    print("C method")
   
objC = C()
objZ = B()
objC.method()
objC.second_method()
objC.third_method()



#inherit 1 level , override
class Animal:
  def __init__(self,name, color) -> None:
    self.name = name
    self.color = color
  
  def run(self):
    print("Animal's running")
  
class Dog(Animal):                #Dog inherits Animal
  def bark(self):
    print("Gau gau")
  def run(self):                  #override method run() in Animal
    print("Dog's running")

class Cat(Animal):                #Cat inherits Animal
  def purr(self):
    print("meo meo")
  def run(self):                  #override method run() in Animal
    print("Cat's running")

dog1 = Dog("Ben", 'blue')
cat1 = Cat("My","red")
print(dog1.name)
dog1.bark()
print(cat1.color)
cat1.purr()
dog1.run()
cat1.run()




#class, self, object(instance), instance attribute, instance method
#self is MUST for all instance method

class Dog:
  legs = 4
  def __init__(self,name,color) -> None:
    self.n = name
    self.c = color
  
  def bark(self):
    print("Gau gau!")

dog1 = Dog("Cun", 'white')
print(f"dog1 name: {dog1.n}" )
print(f"dog1 legs: {dog1.legs}" )
print(f"Dog legs: {Dog.legs}" )
dog1.bark()
Dog.bark(Dog)                     #call bark from class Dog will need param self



class Student:
  def __init__(self,name) -> None:
    self.name = name
  
  def greet(self):                            #all methods must have self as 1st param
    print(self.name + ' says hi')

obj = Student("John")
obj.greet()



class Cat:  
  def __init__(self, color, legs) -> None:    #__init__ is CONSTRUCTOR  #1st param is can be any name but it always refers as self                                              
    self.color = color
    self.legs = legs

felix = Cat('yellow', 4)
jiny = Cat(3, 'brown')
print(felix.color)
print(jiny.color)


"""