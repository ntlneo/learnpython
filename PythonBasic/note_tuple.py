# words = ("I like", "to", "move it", "to do")
# print(words[2])
# print(words.count("to"))    #count exact word
# print(words.index("to do"))
# count = 0
# for word in words:          
#     if "to" in word:        #count ~ word
#         count += 1
# print(count)


#slice with list and tuple
mytuple = (1, "lam", 2, "thanh")
mylist = [3, "an", "my", 4]
# print(mytuple[1:3])
# print(mytuple[::-1])    #reverse
print(mylist[:3])       #lay 3 cai dau tien
print(mylist[-3:])      #lay 3 cai cuoi
# print(mylist[2:])
# print(mylist[::-1])      #reverse

stri = "5 6 7 88 3 2 6"
li = stri.split()
print(li)
z = li[-1:]
print(z[0])     #lay ra the last
print(li[len(li)-1])    #lay ra the last