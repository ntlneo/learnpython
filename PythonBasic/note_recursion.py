
# n! = n * (n-1)! -> 4! = 4*3*2*1 = 24 , 5! = 120

#not use recursion
def calcu_factorial(x):
    result = 1
    for num in range(x,0,-1):
        result *= num
    return result
print(calcu_factorial(5))

#use recursion on 1 function
def factorial(x):
    if x != 1:
        return x * factorial(x-1)
    else:
        return 1
print(factorial(5))


#use recursion from other functions
def is_even(x):
    if x == 0:
        return True
    else:
        return is_odd(x-1)
def is_odd(x):
    return not is_even(x)
print(is_odd(17))
print(is_odd(16))
print(is_even(23))
print(is_even(24))

#fibonacci
def fib(x):
  if x == 0 or x == 1:
    return 1
  else: 
    return fib(x-1) + fib(x-2)
print(fib(4))
print(fib(5))
print(fib(6))