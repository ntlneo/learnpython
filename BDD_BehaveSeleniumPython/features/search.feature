Feature: Search Function

  @smoketest
  Scenario: Search a valid product
    Given I go to Home page
    When I enter valid product into Search box
    And I click on Search button
    Then Valid product should display in Search result

  @negative
  @smoketest
  Scenario: Search an invalid product
    Given I go to Home page
    When I enter invalid product into Search box
    And I click on Search button
    Then Error message 'No product' should display in Search result

  @wip
  @smoketest
  Scenario: Search without entering any product
    Given I go to Home page
    When I don't enter anything into Search box
    And I click on Search button
    Then Error message 'No product' should display in Search result





