from time import sleep
from behave import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

url = "https://tutorialsninja.com/demo/"


@Given("I go to Home page")
def step_impl(context):
    options = webdriver.ChromeOptions()
    # hide DevTools
    # hide Auto bar: chrome being controled...
    options.add_experimental_option(
        "excludeSwitches", ["enable-logging", "enable-automation"]
    )
    options.add_argument("start-maximized")
    options.page_load_strategy = "normal"  # default-wait until page load completely

    # hide log WDM, no need with webdriver-manager 3.9
    # ChromeDriverManager(log_level=0)

    # If you are using Selenium 4.6 or greater, you shouldn’t need to set a driver location.
    # simple run
    # context.driver = webdriver.Chrome(options=options)

    # run with path
    # service = ChromeService(executable_path="/Drivers/chromedriver")
    # context.driver = webdriver.Chrome(service=service, options=options)

    # run with latest Driver auto download. Has error: No such driver version 115
    # https://stackoverflow.com/questions/76724939/there-is-no-such-driver-by-url-https-chromedriver-storage-googleapis-com-lates
    # If you're wondering why you're now seeing this error for ChromeDriverManager,
    # it's because https://chromedriver.chromium.org/downloads only goes up to version 114
    # due to driver restructuring by the Chromium Team for the new Chrome-for-Testing.
    # service = ChromeService()
    # context.driver = webdriver.Chrome(
    #     # service=ChromeService(ChromeDriverManager().install()), options=options
    #     service=service,
    #     options=options,
    # )
    context.driver = webdriver.Chrome(
        service=ChromeService(ChromeDriverManager().install())
    )
    context.driver.implicitly_wait(5)  # ineffective when page load too quick

    # context.driver.maximize_window()
    context.driver.get(url)
    sleep(2)


@When("I enter valid product into Search box")
def step_impl(context):
    search_box = (By.NAME, "search")
    context.driver.find_element(*search_box).send_keys("HP")
    sleep(2)


@When("I click on Search button")
def step_impl(context):
    search_btn = (By.XPATH, "//span[@class='input-group-btn']/button")
    context.driver.find_element(*search_btn).click()
    sleep(2)


@Then("Valid product should display in Search result")
def step_impl(context):
    expected_text = "HP LP3065"
    linktext = (By.LINK_TEXT, expected_text)
    assert context.driver.find_element(*linktext).is_displayed()
    context.driver.quit()


@When("I enter invalid product into Search box")
def step_impl(context):
    search_box = (By.NAME, "search")
    context.driver.find_element(*search_box).send_keys("Alibaba")


@Then("Error message 'No product' should display in Search result")
def step_impl(context):
    error_NoProduct = "There is no product that matches the search criteria."
    error_txt = (By.XPATH, f"//p[contains(text(),'{error_NoProduct}')]")
    assert context.driver.find_element(*error_txt).text == error_NoProduct


@When("I don't enter anything into Search box")
def step_impl(context):
    pass
