from behave import *


@given("I go to Register page")
def step_impl(context):
    pass


@when("I enter valid data into all fields")
def step_impl(context):
    pass


@when("I click Continue button")
def step_impl(context):
    pass


@then("Account should created successfully")
def step_impl(context):
    pass


@when("I enter invalid data into all fields")
def step_impl(context):
    pass


@then("I should get error message about invalid data")
def step_impl(context):
    pass


@when("I enter an existing email")
def step_impl(context):
    pass


@then("I should get error message about duplicate email")
def step_impl(context):
    pass


@then("I should get error message about required fields")
def step_impl(context):
    pass
