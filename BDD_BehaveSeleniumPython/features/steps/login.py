from behave import *


@given("I go to Login page")
def step_impl(context):
    pass


@when("I enter valid email and password in to fields")
def step_impl(context):
    pass


@when("I click Login button")
def step_impl(context):
    pass


@then("I should get logged in")
def step_impl(context):
    pass


@when("I enter valid email and invalid password into fields")
def step_impl(context):
    pass


@then("I should get proper warning message")
def step_impl(context):
    pass


@when("I enter invalid email and password into fields")
def step_impl(context):
    pass


@when("I don't enter anything into fields")
def step_impl(context):
    pass
