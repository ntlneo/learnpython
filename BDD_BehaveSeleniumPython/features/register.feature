Feature: Register Function

    Test negative and positive case

    Scenario: Register with all valid field
        Given I go to Register page
        When I enter valid data into all fields
        And I click Continue button
        Then Account should created successfully

    Scenario: Register with invalid field
        Given I go to Register page
        When I enter invalid data into all fields
        And I click Continue button
        Then I should get error message about invalid data

    Scenario: Register with duplicated email
        Given I go to Register page
        When I enter valid data into all fields
        And I enter an existing email
        And I click Continue button
        Then I should get error message about duplicate email

    Scenario: Register without data
        Given I go to Register page
        When I don't enter anything into fields
        And I click Continue button
        Then I should get error message about required fields



