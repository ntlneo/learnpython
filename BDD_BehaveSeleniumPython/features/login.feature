Feature: Login function

    Test positive and negative case

    Scenario: Login with valid credentials
        Given I go to Login page
        When I enter valid email and password in to fields
        And I click Login button
        Then I should get logged in

    Scenario: Login with valid email and invalid password
        Given I go to Login page
        When I enter valid email and invalid password into fields
        And I click Login button
        Then I should get proper warning message

    Scenario: Login with invalid email and valid password
        Given I go to Login page
        When I enter valid email and invalid password into fields
        And I click Login button
        Then I should get proper warning message

    Scenario: Login with invalid credentials
        Given I go to Login page
        When I enter invalid email and password into fields
        And I click Login button
        Then I should get proper warning message

    Scenario: Login without enter any credentials
        Given I go to Login page
        When I don't enter anything into fields
        And I click Login button
        Then I should get proper warning message